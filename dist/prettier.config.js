"use strict";
module.exports = {
    printWidth: 100,
    singleQuote: true,
    trailingComma: 'all',
    bracketSpacing: true,
    jsxBracketSameLine: false,
    tabWidth: 2,
    semi: false,
    endOfLine: 'lf',
};
//# sourceMappingURL=prettier.config.js.map