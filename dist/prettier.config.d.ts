export declare const printWidth: number;
export declare const singleQuote: boolean;
export declare const trailingComma: string;
export declare const bracketSpacing: boolean;
export declare const jsxBracketSameLine: boolean;
export declare const tabWidth: number;
export declare const semi: boolean;
export declare const endOfLine: string;
