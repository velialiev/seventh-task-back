"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.extractIdFromInsertResult = void 0;
exports.extractIdFromInsertResult = (insertResult) => {
    const { identifiers: [{ id }] } = insertResult;
    return id;
};
//# sourceMappingURL=extractIdFromInsertResultUtil.js.map