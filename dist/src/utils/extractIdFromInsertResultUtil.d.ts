import { InsertResult } from 'typeorm/index';
import { EntityId } from '../types/EntityId';
export declare const extractIdFromInsertResult: (insertResult: InsertResult) => EntityId;
