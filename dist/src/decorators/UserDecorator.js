"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthorizedUser = void 0;
const common_1 = require("@nestjs/common");
exports.AuthorizedUser = common_1.createParamDecorator((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
});
//# sourceMappingURL=UserDecorator.js.map