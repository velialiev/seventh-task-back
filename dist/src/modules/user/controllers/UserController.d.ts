import { UserData, UserService } from '../services/UserService';
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    getCurrent(authorizedUser: UserData): Promise<import("../entities/UserEntity").User | undefined>;
    getById(id: string): Promise<import("../entities/UserEntity").User | undefined>;
}
