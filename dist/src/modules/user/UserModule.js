"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModule = void 0;
const common_1 = require("@nestjs/common");
const UserController_1 = require("./controllers/UserController");
const UserService_1 = require("./services/UserService");
const typeorm_1 = require("@nestjs/typeorm");
const UserEntity_1 = require("./entities/UserEntity");
let UserModule = class UserModule {
};
UserModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                UserEntity_1.User,
            ]),
        ],
        controllers: [UserController_1.UserController],
        providers: [UserService_1.UserService],
        exports: [UserService_1.UserService],
    })
], UserModule);
exports.UserModule = UserModule;
//# sourceMappingURL=UserModule.js.map