import { EntityId } from '../../../types/EntityId';
import { User } from '../entities/UserEntity';
import { Repository } from 'typeorm/index';
import { CreateUserDto } from '../dtos/CreateUserDto';
export declare class UserService {
    private userRepository;
    constructor(userRepository: Repository<User>);
    findByEmail(email: string): Promise<User | undefined>;
    findById(id: EntityId): Promise<User | undefined>;
    create(user: CreateUserDto): Promise<import("typeorm").InsertResult>;
}
export declare type UserData = Omit<User, 'password'>;
