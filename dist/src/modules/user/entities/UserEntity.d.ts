import { EntityId } from '../../../types/EntityId';
export declare class User {
    toJSON(): this;
    id: EntityId;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    dateCreated: number;
}
