export declare class DonationCreateDto {
    accountId: string;
    authorId: string;
    amount: number;
    description: string;
    endDate: string;
    name: string;
    target: string;
    imageName: string;
}
