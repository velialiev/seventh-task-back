import { EntityId } from '../../../types/EntityId';
export declare class Donation {
    id: EntityId;
    accountId: string;
    authorId: string;
    amount: number;
    description: string;
    endDate: string;
    name: string;
    target: string;
    imageName: string;
}
