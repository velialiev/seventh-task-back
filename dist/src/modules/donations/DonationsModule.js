"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DonationsModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const DonationEntity_1 = require("./entities/DonationEntity");
const DonationService_1 = require("./services/DonationService");
const DonationController_1 = require("./controllers/DonationController");
let DonationsModule = class DonationsModule {
};
DonationsModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                DonationEntity_1.Donation,
            ]),
        ],
        providers: [
            DonationService_1.DonationService,
        ],
        controllers: [
            DonationController_1.DonationController,
        ],
    })
], DonationsModule);
exports.DonationsModule = DonationsModule;
//# sourceMappingURL=DonationsModule.js.map