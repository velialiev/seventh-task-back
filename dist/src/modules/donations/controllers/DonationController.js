"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DonationController = void 0;
const common_1 = require("@nestjs/common");
const DonationService_1 = require("../services/DonationService");
const DonationCreateDto_1 = require("../dtos/DonationCreateDto");
const extractIdFromInsertResultUtil_1 = require("../../../utils/extractIdFromInsertResultUtil");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
const fs_1 = require("fs");
const uuid_1 = require("uuid");
const path_1 = __importStar(require("path"));
let DonationController = class DonationController {
    constructor(donationService) {
        this.donationService = donationService;
    }
    find() {
        return this.donationService.find();
    }
    async findById(id) {
        if (!id) {
            throw new common_1.BadRequestException('id isn\'t provided');
        }
        const donation = await this.donationService.findById(+id);
        if (!donation) {
            throw new common_1.NotFoundException('donation not found');
        }
        return donation;
    }
    async create(donationCreateDto, image) {
        const result = await this.donationService.create({
            ...donationCreateDto,
            imageName: image?.filename,
        });
        return {
            id: extractIdFromInsertResultUtil_1.extractIdFromInsertResult(result),
        };
    }
    async getImageByName(name, res) {
        res.sendFile(path_1.default.join(__dirname, '..', '..', '..', '..', '..', 'uploads', name));
    }
};
__decorate([
    common_1.Get('/'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], DonationController.prototype, "find", null);
__decorate([
    common_1.Get('/:id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], DonationController.prototype, "findById", null);
__decorate([
    common_1.Post('/'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('image', {
        storage: multer_1.diskStorage({
            destination: (req, file, cb) => {
                const uploadPath = './uploads';
                if (!fs_1.existsSync(uploadPath)) {
                    fs_1.mkdirSync(uploadPath);
                }
                cb(null, uploadPath);
            },
            filename: (req, file, cb) => {
                cb(null, `${uuid_1.v4()}${path_1.extname(file.originalname)}`);
            },
        }),
    })),
    __param(0, common_1.Body()),
    __param(1, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [DonationCreateDto_1.DonationCreateDto, Object]),
    __metadata("design:returntype", Promise)
], DonationController.prototype, "create", null);
__decorate([
    common_1.Get('/image/:name'),
    __param(0, common_1.Param('name')), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], DonationController.prototype, "getImageByName", null);
DonationController = __decorate([
    common_1.Controller('/donation'),
    __metadata("design:paramtypes", [DonationService_1.DonationService])
], DonationController);
exports.DonationController = DonationController;
//# sourceMappingURL=DonationController.js.map