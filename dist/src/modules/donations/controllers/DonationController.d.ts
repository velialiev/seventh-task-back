import { DonationService } from '../services/DonationService';
import { DonationCreateDto } from '../dtos/DonationCreateDto';
import { Response } from 'express';
export declare class DonationController {
    private donationService;
    constructor(donationService: DonationService);
    find(): Promise<import("../entities/DonationEntity").Donation[]>;
    findById(id: string): Promise<import("../entities/DonationEntity").Donation>;
    create(donationCreateDto: DonationCreateDto, image: any): Promise<{
        id: number;
    }>;
    getImageByName(name: string, res: Response): Promise<void>;
}
