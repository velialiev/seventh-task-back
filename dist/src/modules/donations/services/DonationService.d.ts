import { Donation } from '../entities/DonationEntity';
import { Repository } from 'typeorm';
import { DonationCreateDto } from '../dtos/DonationCreateDto';
export declare class DonationService {
    private donationRepository;
    constructor(donationRepository: Repository<Donation>);
    find(): Promise<Donation[]>;
    findById(id: number): Promise<Donation | undefined>;
    create(donationCreateDto: DonationCreateDto): Promise<import("typeorm").InsertResult>;
}
